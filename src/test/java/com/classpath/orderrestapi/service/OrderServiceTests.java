package com.classpath.orderrestapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.classpath.orderrestapi.dto.OrderDto;
import com.classpath.orderrestapi.model.Order;
import com.classpath.orderrestapi.repository.OrderRepository;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTests {

	@InjectMocks
	private OrderService orderService;

	@Mock
	private OrderRepository orderRepository;

	@Test
	void testSaveOrder() {
		/*
		 * 1. Set the expectations on the mock object 2. execute the method under test
		 * 3. Assert the output 4. Verify the expectations on the mock object
		 */

		Order response = Order.builder().id(12L).name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now())
				.price(340D).build();
		Order input = Order.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
				.build();

		// setting the expections on the mock object
		when(this.orderRepository.save(any())).thenReturn(response);

		// execute
		Order savedOrder = this.orderService.save(input);

		// assert
		Assertions.assertNotNull(savedOrder);
		Assertions.assertEquals(12L, response.getId());
		Assertions.assertEquals("ramesh@gmail.com", savedOrder.getEmail());

		// verification
		verify(this.orderRepository, times(1)).save(any());
	}

	@Test
	void testValidOrderIdFetch() {

		Order response = Order.builder().id(12L).name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now())
				.price(340D).build();
		// setting the expections on the mock object
		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(response));

		Order order = this.orderService.fetchOrderById(12L);
		Assertions.assertNotNull(order);

		// verification
		verify(this.orderRepository, times(1)).findById(anyLong());
	}

	@Test
	void testInValidOrderIdFetch() {

		Order response = Order.builder().id(12L).name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now())
				.price(340D).build();
		// setting the expections on the mock object
		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));

		try {
			this.orderService.fetchOrderById(12L);
			fail("Thie method should throw IllegalArgumentException");
		} catch (Exception exception) {
			assertNotNull(exception);
			assertTrue(exception instanceof IllegalArgumentException);
		}

		// verification
		verify(this.orderRepository, times(1)).findById(anyLong());
	}
	

	@Test
	void testInValidOrderIdFetch2() {

		Order response = Order.builder().id(12L).name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now())
				.price(340D).build();
		// setting the expections on the mock object
		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));

		assertThrows(IllegalArgumentException.class, () -> {
			this.orderService.fetchOrderById(12L);
		});
		// verification
		verify(this.orderRepository, times(1)).findById(anyLong());
	}
	
	@Test
	void testDeleteOrderById() {
		
		doNothing().when(this.orderRepository).deleteById(anyLong());
		
		this.orderService.deleteOrderById(12L);
		
		// verification
		verify(this.orderRepository, times(1)).deleteById(anyLong());
	}
	
	@Test
	void testFetchOrdersByPriceRange() {
		// setting the expections on the mock object
		when(this.orderRepository.findByPriceBetween(anyDouble(), anyDouble()))
			.thenReturn(List.of(mock(OrderDto.class), mock(OrderDto.class)));
		
		List<OrderDto> listOfOrders = this.orderService.fetchAllOrdersByPriceRange(230d, 400d);
		
		assertNotNull(listOfOrders);
		assertEquals(2, listOfOrders.size());
		
		// verification
		verify(this.orderRepository, times(1)).findByPriceBetween(anyDouble(), anyDouble());
		
	}
	

}
