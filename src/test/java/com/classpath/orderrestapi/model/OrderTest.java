package com.classpath.orderrestapi.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import org.junit.jupiter.api.Test;

public class OrderTest {
	
	@Test
	void testOrderConstructor() {
		/*
		 * 1. Creating the object
		 * 2. assert the properties
		 */
		
		Order order = Order
						.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
						.build();
		assertNotNull(order);
		assertEquals("Ramesh", order.getName());
		assertEquals("ramesh@gmail.com", order.getEmail());
		assertEquals(340D, order.getPrice());
		assertEquals(LocalDate.now(), order.getOrderDate());
	}
	
	@Test
	void testOrderToString() {
		/*
		 * 1. Creating the object
		 * 2. Optionally call the methods
		 * 3. assert the properties
		 */
		
		Order order = Order
						.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
						.build();
		assertNotNull(order);
		assertEquals("Order(id=null, name=Ramesh, email=ramesh@gmail.com, price=340.0, orderDate=2022-06-30, lineItems=null)", order.toString());
	}
	
	@Test
	void testOrderEquals() {
		/*
		 * 1. Creating the object
		 * 2. assert the properties
		 */
		
		Order order1 = Order
						.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
						.build();
		Order order2 = Order
				.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
				.build();
		
		assertTrue(order1 != order2);
		assertEquals(order1, order2);
	}
	
	
	@Test
	void testOrderHashCode() {
		/*
		 * 1. Creating the object
		 * 2. assert the properties
		 */
		
		Order order1 = Order
						.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
						.build();
		Order order2 = Order
				.builder().name("Ramesh").email("ramesh@gmail.com").orderDate(LocalDate.now()).price(340D)
				.build();
		
		assertTrue(order1 != order2);
		assertEquals(order1.hashCode(), order2.hashCode());
	}


}
